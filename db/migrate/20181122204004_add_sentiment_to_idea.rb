class AddSentimentToIdea < ActiveRecord::Migration[5.2]
  def change
    add_column :ideas, :sentiment, :string
  end
end
