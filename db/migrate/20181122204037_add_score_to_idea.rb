class AddScoreToIdea < ActiveRecord::Migration[5.2]
  def change
    add_column :ideas, :score, :decimal
  end
end
