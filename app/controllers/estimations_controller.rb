class EstimationsController < ApplicationController
    before_action :set_estimation, only: [:show, :edit, :destroy]
# GET /estimations
  # GET /estimations.json
    def index
    @estimations = Estimation.all
      end
    
     def create
    contributor = Contributor.find(params[:contributor_id])
    story_points = params[:story_points].to_i
    vote = contributor.team.current_vote
    contributor.estimate(vote, points: story_points)
    redirect_to estimations_url
  end
    
      # GET /estimations/1
  # GET /estimations/1.json
      def show
  
      end


  # DELETE /ideas/1
  # DELETE /ideas/1.json
  def destroy
    @estimation.destroy
    respond_to do |format|
      format.html { redirect_to estimations_url, notice: 'Estimation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # GET /ideas/1/edit
  def edit
  end
      
    private
        def set_estimation
      @estimation = Estimation.find(params[:id])
    end

    def estimation_params
      params.require(:estimation).permit(:story_points, :contributor)
    end
end