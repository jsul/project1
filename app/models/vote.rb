class Vote < ApplicationRecord
  belongs_to :team
    has_many :estimations
  validates :ending, presence: true

  def seconds_to_end
    seconds = ((ending.to_datetime - DateTime.current) * 1.days).to_i
    [seconds, 0].max
  end

  def running?
    DateTime.current < self.ending
  end
  
 def story_points
    estimations.group_by(&:contributor)
               .values
               .map { |estims| estims.max_by(&:created_at) }
               .map(&:story_points)
  end
end
