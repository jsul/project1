class Team < ApplicationRecord
has_many :contributors
    has_many :votes
    belongs_to :current_vote, class_name: "Vote", optional: true
    validate :current_vote_must_be_a_vote
    
    def start_vote(ending)
    vote = self.votes.create(ending: ending)
    self.current_vote = vote
    self.save
    vote
  end
    private
    def current_vote_must_be_a_vote
    if !current_vote.nil? and !votes.include?(current_vote)
      errors.add(:current_vote, "must be part of the team's votes")
    end
  end
end


