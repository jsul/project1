class Idea < ApplicationRecord
    acts_as_list scope: :retro
  belongs_to :retro
  validates :name, presence: true

 
  
  before_save :set_sentiment, if: :name_changed?
  
  scope :positive, ->{ where(sentiment: :positive) }
  scope :neutral, ->{ where(sentiment: :neutral) }
  scope :negative, ->{ where(sentiment: :negative) }
  
  def set_sentiment
       self.sentiment = $analyzer.sentiment(name)
       self.score = $analyzer.score(name)
  end
end

