class Retro < ApplicationRecord
    acts_as_list
    has_many :ideas, ->{ order(position: :asc) }, dependent: :destroy
    validates :name, presence: true
end
