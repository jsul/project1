class Contributor < ApplicationRecord
  belongs_to :team
    has_many :estimations
    
    def team_name
        team && team.name
    end
    
    def estimate(vote, points:)
        estimations.create(vote: vote, story_points: points)
    end
end
