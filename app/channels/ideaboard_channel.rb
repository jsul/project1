class IdeaboardChannel < ApplicationCable::Channel
    def subscribed
        stream_from "ideaboard"
        # stream_from "some_channel"
    end
    
    def unsubscribed
        stream_from "ideaboard"
    end
end

