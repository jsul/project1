class BoardChannel < ApplicationCable::Channel
  def subscribed
      stream_from "board"
    # stream_from "some_channel"
  end

  def unsubscribed
    stream_from "board"
  end
end
