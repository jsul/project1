/* eslint no-console:0 */
// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.
//
// To reference this file, add <%= javascript_pack_tag 'application' %> to the appropriate
// layout file, like app/views/layouts/application.html.erb

import Vue from 'vue/dist/vue.esm'

import Vuex from 'vuex'
import Retroapp from '../retroapp.vue'

import TurbolinksAdapter from 'vue-turbolinks'
import Rails from 'rails-ujs'

Vue.use(Vuex)
Vue.use(TurbolinksAdapter)


window.stores = new Vuex.Store({
    state: {
        retros: []
    }, 
    mutations: {
    addRetro(state, data) {
      state.retros.push(data)
    },
    moveRetro(state, data) {
      const index = state.retros.findIndex(item => item.id == data.id)
      state.retros.splice(index, 1)
      state.retros.splice(data.position - 1, 0, data)
    },
    addIdea(state, data) {
      const index = state.retros.findIndex(item => item.id == data.retro_id)
      state.retros[index].ideas.push(data)
    },
    editIdea(state, data) {
      const retro_index = state.retros.findIndex((item) => item.id == data.retro_id)
      const idea_index = state.retros[retro_index].ideas.findIndex((item) => item.id == data.id)
      state.retros[retro_index].ideas.splice(idea_index, 1, data)
    },

    moveIdea(state, data) {
      const old_retro_index = state.retros.findIndex((retro) => {
        return retro.ideas.find((idea) => {
          return idea.id === data.id
        })
      })
      const old_idea_index = state.retros[old_retro_index].ideas.findIndex((item) => item.id == data.id)
      const new_retro_index = state.retros.findIndex((item) => item.id == data.retro_id)

      if (old_retro_index != new_retro_index) {
        // Remove idea from old retro, add to new one
        state.retros[old_retro_index].ideas.splice(old_idea_index, 1)
        state.retros[new_retro_index].ideas.splice(data.position - 1, 0, data)
      } else {
        state.retros[new_retro_index].ideas.splice(old_idea_index, 1)
        state.retros[new_retro_index].ideas.splice(data.position - 1, 0, data)
      }
    }
  }
})
document.addEventListener("turbolinks:load", function() {
                          var element = document.querySelector("#ideaboards")
                          if (element != undefined) {
                          window.stores.state.retros = JSON.parse(element.dataset.retros)
                          
                          var retroapp = new Vue({
                                              el: element,
                                              store: window.stores,
                                              template: "<Retroapp />",
                                              components: { Retroapp }
                                              })
                          }
                          });
