App.ideaboard = App.cable.subscriptions.create "IdeaboardChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (rdata) ->
    # Called when there's incoming data on the websocket for this channel
    console.log(rdata)
    if rdata.commit
      window.stores.commit(rdata.commit, JSON.parse(rdata.payload))
