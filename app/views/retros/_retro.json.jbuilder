json.extract! retro, :id, :name, :position, :created_at, :updated_at, :ideas
json.url retro_url(retro, format: :json)
