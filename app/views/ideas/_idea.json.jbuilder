json.extract! idea, :id, :retro_id, :name, :position, :created_at, :updated_at
json.url idea_url(idea, format: :json)
