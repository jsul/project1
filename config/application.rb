require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Project
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
   # config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
       # Track comments annotations on other file formats
    config.annotations.register_extensions("scss", "sass", "less", "coffee") { |tag| /\/\/\s*(#{tag}):?\s*(.*)$/ }

    config.active_record.sqlite3.represent_boolean_as_integer = true
  end
  
    
     TITLE = "AgiliT"
  KEYWORDS = "AgiliT"
  FIBOS = [0, 1, 2, 3, 5, 8, 13, 21, 34]
end
