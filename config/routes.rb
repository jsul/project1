
Rails.application.routes.draw do
 root 'static_pages#home'
 
  get 'site/about'
  get 'site/contact'
    
  
    
    get '/about' => 'site#about'
    get '/contact' => 'site#contact'
    get '/retros' => 'retros#index'
    get '/lists' => 'lists#index'
    get '/estimations' => 'estimations#index'
   # Serve websocket cable requests in-process
  mount ActionCable.server => '/cable'

resources :contributors, only: [:create, :show] do
    resources :estimations, only: :create
  end

  resources :teams, only: [] do
    resource :votes, only: :create
  end
    
    
resources :lists do
        member do
             patch :move
            end
            end
  resources :cards do
        member do
        patch :move
        end
    end
   
    
  resources :retros do
      member do
          patch :move
      end
  end
  resources :ideas do
      member do
          patch :move
      end
  end


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
